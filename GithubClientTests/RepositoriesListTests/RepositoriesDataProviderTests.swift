//
//  RepositoriesDataProviderTests.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import XCTest
@testable import GithubClient

class RepositoriesDataProviderTests: XCTestCase {
    
    let service = RepositoriesNetworkServiceMock(requestManager: RequestManagerMock())
    var dataProvider: RepositoriesDataProvider!
    
    override func setUp() {
        dataProvider = RepositoriesDataProvider(service)
    }
    
    func testLoadByLoginFailure() {
        
        let serviceFilure = RepositoriesNetworkServiceMock(requestManager: RequestManagerFailureMock())
        let dataProviderFilure = RepositoriesDataProvider(serviceFilure)
        
        let exp = self.expectation(description: "testLoadByLoginFailure")
        
        dataProviderFilure.loadRepositories(login: "nazargorobets") { (data, error) in
            XCTAssertNil(data)
            XCTAssertNotNil(error)
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5)
    }
    
    func testLoadByNameFailure() {
        
        let serviceFilure = RepositoriesNetworkServiceMock(requestManager: RequestManagerFailureMock())
        let dataProviderFilure = RepositoriesDataProvider(serviceFilure)
        
        let exp = self.expectation(description: "testLoadByNameFailure")
        
        dataProviderFilure.loadRepositories(name: "nazargorobets") { (data, error) in
            
            XCTAssertNil(data)
            XCTAssertNotNil(error)
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5)
    }

    func testLoadRepositoriesbyLogin() {
        
        let exp = self.expectation(description: "testLoadRepositoriesbyLogin")
        
        dataProvider.loadRepositories(login: "nazargorobets") { (data, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            XCTAssertEqual(data?.count, 1)
            
            let first = data?.first
            
            XCTAssertNotNil(first)
            XCTAssertNotNil(first?.ownerImage)
            XCTAssertNotNil(first?.repoName)
            XCTAssertNotNil(first?.repoDescription)
            XCTAssertNotNil(first?.watchersCount)
            XCTAssertNotNil(first?.forksCount)
            XCTAssertNotNil(first?.forks_url)
            
            XCTAssertEqual(first?.ownerImage, "https://avatars1.githubusercontent.com/u/7430773?v=4")
            XCTAssertEqual(first?.repoName, "VK")
            XCTAssertEqual(first?.repoDescription, "VK app")
            XCTAssertEqual(first?.watchersCount, 0)
            XCTAssertEqual(first?.forksCount, 0)
            XCTAssertEqual(first?.forks_url, "https://api.github.com/repos/NazarGorobets/VK/forks")
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5)
    }
    
    func testLoadRepositoriesbyName() {
        
        let exp = self.expectation(description: "testLoadRepositoriesbyName")
        
        dataProvider.loadRepositories(name: "nazargorobets") { (data, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            XCTAssertEqual(data?.count, 2)
            
            let first = data?.first
            
            XCTAssertNotNil(first)
            XCTAssertNotNil(first?.ownerImage)
            XCTAssertNotNil(first?.repoName)
            XCTAssertNotNil(first?.repoDescription)
            XCTAssertNotNil(first?.watchersCount)
            XCTAssertNotNil(first?.forksCount)
            XCTAssertNotNil(first?.forks_url)
            
            XCTAssertEqual(first?.ownerImage, "https://avatars3.githubusercontent.com/u/5394955?v=4")
            XCTAssertEqual(first?.repoName, "sentrygun")
            XCTAssertEqual(first?.repoDescription, "Rogue AP killer")
            XCTAssertEqual(first?.watchersCount, 53)
            XCTAssertEqual(first?.forksCount, 28)
            XCTAssertEqual(first?.forks_url, "https://api.github.com/repos/s0lst1c3/sentrygun/forks")
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5)
    }
}
