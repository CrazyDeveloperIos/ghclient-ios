//
//  RepositoriesListPresenterTest.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import XCTest
@testable import GithubClient

class RepositoriesListPresenterTest: XCTestCase {

    var presenter = RepositoriesListPresenter()
    var interactor: RepositoriesListInteractor = RepositoriesListInteractor()
    private var networkMock = RepositoriesNetworkServiceMock(requestManager: RequestManagerMock())
    
    override func setUp() {
        interactor.set(RepositoriesDataProvider(networkMock))
        interactor.set(AuthorizedMock())
        presenter.interactor = interactor
        presenter.wireframe = RepositoriesListWireframeMock()
        presenter.view = RepositoriesListViewMock()
    }
    
    func testLoadAndReset() {
        
        XCTAssertEqual(presenter.dataSource.count, 0)
        
        presenter.load()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.presenter.dataSource.count, 1)
            self.presenter.reset()
            XCTAssertEqual(self.presenter.dataSource.count, 0)
        }
    }
    
    func testLoadByNameAndReset() {
        
        XCTAssertEqual(presenter.dataSource.count, 0)
        
        presenter.load(name: "nazargorobets")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.presenter.dataSource.count, 2)
            self.presenter.reset()
            XCTAssertEqual(self.presenter.dataSource.count, 0)
        }
    }
    
    func testSwitchType() {
        
        presenter.moduleType = .global
        XCTAssertEqual(presenter.dataSource.count, 0)
        
        presenter.load()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.presenter.dataSource.count, 0)
        }
    }
    
    func testSwitchTypeMy() {
        
        presenter.moduleType = .my
        
        XCTAssertEqual(presenter.dataSource.count, 0)
        
        presenter.load()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.presenter.dataSource.count, 1)
            self.presenter.reset()
            XCTAssertEqual(self.presenter.dataSource.count, 0)
        }
    }
    
    func testShowLogin() {
        interactor.set(NotAuthorizedMock())
        presenter.showOwnList()
    }
}
