//
//  RepositoriesListViewMock.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

@testable import GithubClient

class RepositoriesListViewMock: RepositoriesListViewProtocol {
    
    func reload() { }
    
    func showLoader() { }
    
    func dismissLoader() { }
    
}
