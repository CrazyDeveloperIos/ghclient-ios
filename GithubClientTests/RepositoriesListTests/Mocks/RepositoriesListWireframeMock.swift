//
//  RepositoriesListWireframeMock.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
import XCTest

@testable import GithubClient

class RepositoriesListWireframeMock: XCTestCase,  RepositoriesListWireframeProtocol {
    
    var navigationController: UINavigationController!
    
    func build(parameter: AnyObject?) -> UIViewController {
        return UIViewController()
    }
    
    func showDetail(_ repository: RepositoryDTO) {
        XCTAssertNotNil(repository.repoName)
        print(repository.repoName ?? "")
    }
    
    func showOwnList() {
        print("showLogin")
    }
    
    func showLogin(delegate: LoginDelegate?) {
        print("showLogin")
        XCTAssertNotNil(delegate)
        delegate?.onSuccessLogin?()
    }
}
