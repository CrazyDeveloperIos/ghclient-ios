//
//  RequestManagerFailureMock.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

@testable import GithubClient

class RequestManagerFailureMock: RequestManager {
    
    override func request<T>(url: String, method: HTTPMethod, parameters: [String : String]?, headers: [String : String]?, completionHandler: @escaping (T?, NSError?) -> Void) where T : Decodable {
        completionHandler(nil, NSError(domain: "Bad Responce", code: 0, userInfo: nil))
    }
}
