//
//  RepositoriesListInteractorTests.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import XCTest
@testable import GithubClient

class RepositoriesListInteractorTests: XCTestCase {

    var interactor: RepositoriesListInteractor = RepositoriesListInteractor()
    private var networkMock = RepositoriesNetworkServiceMock(requestManager: RequestManagerMock())
    
    override func setUp() {
        interactor.set(RepositoriesDataProvider(networkMock))
    }

    func testAuthorizedUser() {
        interactor.set(AuthorizedMock())
        XCTAssertTrue(interactor.isAuthorizedUser())
    }
    
    func testNotAuthorizedUser() {
        interactor.set(NotAuthorizedMock())
        XCTAssertFalse(interactor.isAuthorizedUser())
    }
    
    func testLoadReposByName() {
        
        let exp = self.expectation(description: "testLoadReposByName")
        
        interactor.loadRepositories(name: "nazargorobets") { (data, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            XCTAssertEqual(data?.count, 2)
            
            let first = data?.first
            
            XCTAssertNotNil(first)
            XCTAssertNotNil(first?.ownerImage)
            XCTAssertNotNil(first?.repoName)
            XCTAssertNotNil(first?.repoDescription)
            XCTAssertNotNil(first?.watchersCount)
            XCTAssertNotNil(first?.forksCount)
            XCTAssertNotNil(first?.forks_url)
            
            XCTAssertEqual(first?.ownerImage, "https://avatars3.githubusercontent.com/u/5394955?v=4")
            XCTAssertEqual(first?.repoName, "sentrygun")
            XCTAssertEqual(first?.repoDescription, "Rogue AP killer")
            XCTAssertEqual(first?.watchersCount, 53)
            XCTAssertEqual(first?.forksCount, 28)
            XCTAssertEqual(first?.forks_url, "https://api.github.com/repos/s0lst1c3/sentrygun/forks")
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5)
    }
    
    func testLoadOwn() {
        
        let exp = self.expectation(description: "testLoadReposByName")
        
        interactor.set(AuthorizedMock())
        
        interactor.loadRepositories { (data, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            XCTAssertEqual(data?.count, 1)
            
            let first = data?.first
            
            XCTAssertNotNil(first)
            XCTAssertNotNil(first?.ownerImage)
            XCTAssertNotNil(first?.repoName)
            XCTAssertNotNil(first?.repoDescription)
            XCTAssertNotNil(first?.watchersCount)
            XCTAssertNotNil(first?.forksCount)
            XCTAssertNotNil(first?.forks_url)
            
            XCTAssertEqual(first?.ownerImage, "https://avatars1.githubusercontent.com/u/7430773?v=4")
            XCTAssertEqual(first?.repoName, "VK")
            XCTAssertEqual(first?.repoDescription, "VK app")
            XCTAssertEqual(first?.watchersCount, 0)
            XCTAssertEqual(first?.forksCount, 0)
            XCTAssertEqual(first?.forks_url, "https://api.github.com/repos/NazarGorobets/VK/forks")
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5)
    }
    
    func testFailureLoadOwn() {
        
        let exp = self.expectation(description: "testFailureLoadOwn")
        
        interactor.set(NotAuthorizedMock())
        
        interactor.loadRepositories { (data, error) in
            
            XCTAssertNil(data)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.domain, "User is not logged in")
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5)
    }
}
