//
//  RepositoriesNetworkServiceTests.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import XCTest
@testable import GithubClient

class RepositoriesNetworkServiceTests: XCTestCase {

    let service = RepositoriesNetworkService(requestManager: RequestManagerMock())
    
    func testLoadByRepoName() {
        
        let exp = self.expectation(description: "testLoadByRepoName")
        
        service.loadRepositories(name: "nazargorobets") { (data, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            XCTAssertEqual(data?.items?.count, 2)
            
            let first = data?.items?.first
            
            XCTAssertNotNil(first)
            XCTAssertNotNil(first?.name)
            XCTAssertNotNil(first?.owner)
            XCTAssertNotNil(first?.owner?.login)
            XCTAssertNotNil(first?.owner?.avatar_url)
            XCTAssertNotNil(first?.description)
            XCTAssertNotNil(first?.watchers_count)
            XCTAssertNotNil(first?.forks_count)
            XCTAssertNotNil(first?.forks_url)
            
            XCTAssertEqual(first?.name, "sentrygun")
            XCTAssertEqual(first?.forks_count, 28)
            XCTAssertEqual(first?.description, "Rogue AP killer")
            XCTAssertEqual(first?.watchers_count, 53)
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 10)
    }
    
    func testLoadByLogin() {
        
        let exp = self.expectation(description: "testLoadByLogin")
    
        service.loadRepositories(login: "nazargorobets") { (items, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(items)
            XCTAssertEqual(items?.count, 1)
            
            let first = items?.first
            XCTAssertNotNil(first)
            XCTAssertNotNil(first?.owner)
            XCTAssertNotNil(first?.owner?.login)
            XCTAssertEqual(first?.owner?.login?.lowercased(), "nazargorobets")
            XCTAssertNotNil(first?.owner?.avatar_url)
            XCTAssertNotNil(first?.description)
            XCTAssertNotNil(first?.watchers_count)
            XCTAssertNotNil(first?.forks_count)
            XCTAssertNotNil(first?.forks_url)
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 10)
    }
}
