//
//  RepositoryDTOTests.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import XCTest
@testable import GithubClient

class RepositoryDTOTests: XCTestCase {

    let service = RepositoriesNetworkService(requestManager: RequestManagerMock())

    func testDTOObject() {
        
        let exp = self.expectation(description: "testExample")
        
        service.loadRepositories(name: "nazargorobets") { (data, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            XCTAssertEqual(data?.items?.count, 2)
            
            let first = data?.items?.first
            
            XCTAssertNotNil(first)
            XCTAssertNotNil(first?.name)
            XCTAssertNotNil(first?.owner)
            XCTAssertNotNil(first?.owner?.login)
            XCTAssertNotNil(first?.owner?.avatar_url)
            XCTAssertNotNil(first?.description)
            XCTAssertNotNil(first?.watchers_count)
            XCTAssertNotNil(first?.forks_count)
            XCTAssertNotNil(first?.forks_url)
            
            guard let item = first else { fatalError("first is nil")}
            
            let repoDTO = RepositoryDTO(item)
            
            XCTAssertEqual(first?.name, repoDTO.repoName)
            XCTAssertEqual(first?.forks_url, repoDTO.forks_url)
            XCTAssertEqual(first?.forks_count, repoDTO.forksCount)
            XCTAssertEqual(first?.owner?.avatar_url, repoDTO.ownerImage)
            XCTAssertEqual(first?.description, repoDTO.repoDescription)
            XCTAssertEqual(first?.watchers_count, repoDTO.watchersCount)

            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 10)
    }
}
