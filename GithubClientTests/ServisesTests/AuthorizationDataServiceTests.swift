//
//  AuthorizationDataServiceTests.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import XCTest
@testable import GithubClient

class AuthorizationDataServiceTests: XCTestCase {

    func testToken() {
        let service = AuthorizationDataService()
        XCTAssertNil(service.token())
        let token = "testToken"
        service.save(token: token)
        XCTAssertEqual(token, service.token())
        service.delete(token: token)
        XCTAssertNil(service.token())
    }
    
    func testLogin() {
        let service = AuthorizationDataService()
        XCTAssertNil(service.login())
        let login = "NazarGorobets"
        service.save(login: login)
        XCTAssertEqual(login, service.login())
        service.delete(login: login)
        XCTAssertNil(service.login())
    }
}
