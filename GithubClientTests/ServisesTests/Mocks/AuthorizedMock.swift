//
//  AuthorizationDataServiceMock.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
@testable import GithubClient

class AuthorizedMock: NSObject, AuthorizationServiceProtocol {
    
    func isAuthorizedUser() -> Bool {
        return true
    }
    
    func token() -> String? {
        return "test_token"
    }
    
    func login() -> String? {
        return "nazargorobets"
    }
    
    @discardableResult func save(token: String) -> Bool {
        return true
    }
    
    @discardableResult func save(login: String) -> Bool {
        return true
    }
}
