//
//  NotAuthorizedMock.swift
//  GithubClientTests
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
@testable import GithubClient

class NotAuthorizedMock: NSObject, AuthorizationServiceProtocol {
    
    func isAuthorizedUser() -> Bool {
        return false
    }
    
    func token() -> String? {
        return nil
    }
    
    func login() -> String? {
        return nil
    }
    
    @discardableResult func save(token: String) -> Bool {
        return false
    }
    
    @discardableResult func save(login: String) -> Bool {
        return false
    }
}
