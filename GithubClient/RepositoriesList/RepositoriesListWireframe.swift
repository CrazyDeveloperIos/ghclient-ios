//
//  RepositoriesListWireframe.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol RepositoriesListWireframeProtocol: BaseWireframe {
    func showDetail(_ repository: RepositoryDTO)
    func showOwnList()
    func showLogin(delegate: LoginDelegate?)
}

public class RepositoriesListWireframe: NSObject, RepositoriesListWireframeProtocol {

    public var navigationController: UINavigationController!
    
    public func build(parameter: AnyObject? = nil) -> UIViewController {
        
        let viewController = startViewController()
        let presenter = RepositoriesListPresenter()
        let interactor = RepositoriesListInteractor()
        
        viewController.presenter = presenter
        
        if let type = parameter as? ModuleType {
            presenter.moduleType = type
        }
        
        presenter.wireframe = self
        presenter.interactor = interactor
        presenter.view = viewController

        return viewController
    }
    
    private func startViewController() -> RepositoriesListViewController {
        let storyboard = UIStoryboard(name: "RepositoriesList", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "RepositoriesListViewController") as! RepositoriesListViewController
    }
    
    public func showDetail(_ repository: RepositoryDTO) {
        var wireframe: RepositoryDetailWireframeProtocol = RepositoryDetailWireframe()
        wireframe.navigationController = navigationController
        navigationController.show(wireframe.build(parameter: repository), sender: nil)
    }
    
    public func showOwnList() {
        var wireframe: RepositoriesListWireframeProtocol = RepositoriesListWireframe()
        wireframe.navigationController = navigationController
        navigationController.show(wireframe.build(parameter: ModuleType.my as AnyObject), sender: nil)
    }
    
    public func showLogin(delegate: LoginDelegate?) {
        var wireframe: LoginWireframeProtocol = LoginWireframe()
        let vc = wireframe.build(parameter: delegate)
        let loginNavigationController = UINavigationController(rootViewController: vc)
        wireframe.navigationController = loginNavigationController
        loginNavigationController.definesPresentationContext = true
        loginNavigationController.modalPresentationStyle = .overCurrentContext
        navigationController.present(loginNavigationController, animated: true, completion: nil)
    }
}
