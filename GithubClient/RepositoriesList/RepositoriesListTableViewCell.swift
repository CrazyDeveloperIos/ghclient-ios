//
//  RepositoriesListTableViewCell.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoriesListTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var watchers: UILabel!
    @IBOutlet weak var forks: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        icon.circle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(repo: RepositoryDTO) {
        
        self.title.text = repo.repoName
        self.subtitle.text = repo.repoDescription
        self.watchers.text = repo.watchersCount.description
        self.forks.text = repo.forksCount.description
        
        guard let urlStr = repo.ownerImage,
            let url = URL(string: urlStr) else {
            icon.image = UIImage(named: "dev")
            return
        }
        
        icon?.sd_setImage(with: url, placeholderImage: UIImage(named: "dev"), options: .progressiveDownload)
    }
}
