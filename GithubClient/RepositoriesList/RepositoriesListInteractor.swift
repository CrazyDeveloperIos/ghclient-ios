//
//  RepositoriesListInteractor.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol RepositoriesListInteractorProtocol {
    func loadRepositories(name: String, completionHandler: @escaping ([RepositoryDTO]?, NSError?) -> Void)
    func loadRepositories(completionHandler: @escaping ([RepositoryDTO]?, NSError?) -> Void)
    func isAuthorizedUser() -> Bool
}

final class RepositoriesListInteractor: NSObject, RepositoriesListInteractorProtocol {
    
    private var repositoriesDataProvider: RepositoriesDataProviderProtocol = RepositoriesDataProvider()
    private var authorizationService: AuthorizationServiceProtocol = AuthorizationService()
    
    public func set(_ dataProvider: RepositoriesDataProviderProtocol) {
        repositoriesDataProvider = dataProvider
    }
    
    public func set(_ service: AuthorizationServiceProtocol) {
        authorizationService = service
    }
    
    public func loadRepositories(name: String, completionHandler: @escaping ([RepositoryDTO]?, NSError?) -> Void) {
        repositoriesDataProvider.loadRepositories(name: name) { (items, error) in
            completionHandler(items, error)
        }
    }
    
    public func loadRepositories(completionHandler: @escaping ([RepositoryDTO]?, NSError?) -> Void) {
        
        guard let login = authorizationService.login() else {
            completionHandler(nil, NSError(domain: "User is not logged in", code: 0, userInfo: nil))
            return
        }
        
        repositoriesDataProvider.loadRepositories(login: login) { (items, error) in
            completionHandler(items, error)
        }
    }
    
    public func isAuthorizedUser() -> Bool {
        return authorizationService.isAuthorizedUser()
    }
}
