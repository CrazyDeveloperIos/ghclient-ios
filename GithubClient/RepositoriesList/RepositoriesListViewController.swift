//
//  RepositoriesListController.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

protocol RepositoriesListViewProtocol: Loader {
    func reload()
}

final class RepositoriesListViewController: UITableViewController, RepositoriesListViewProtocol {
    
    var presenter: RepositoriesListPresenter!
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        definesPresentationContext = true
        tableView.tableFooterView = UIView()
        presenter.loadDataIfNeeded()
    }
    
    public func reload() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    private func setupNavigationBar() {
        title()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        
        if presenter.moduleType == .global {
            setupProfileBarItem()
        }
    }
    
    private func setupProfileBarItem() {
        let item = UIBarButtonItem(title: "My", style: .plain, target: self, action: #selector(ownOwnList))
        navigationItem.setRightBarButton(item, animated: true)
    }
    
    private func title() {
        switch presenter.moduleType {
        case .global:
            title = "All Repositories"
        case .my:
            title = "My"
        }
    }
    
    @objc private func ownOwnList() {
        presenter.showOwnList()
    }
}

    // MARK: - Table view data source

extension RepositoriesListViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoriesListTableViewCell", for: indexPath) as! RepositoriesListTableViewCell
        guard let item = presenter.dataSource[safe: indexPath.row] else { return cell }
        cell.setup(repo: item)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = presenter.dataSource[safe: indexPath.row] else { return }
        presenter.showDetail(item)
    }
}

    // MARK: - Search bar delegatel

extension RepositoriesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 1 {
            presenter.load(name: searchText)
        } else {
            presenter.reset()
        }
    }
}
