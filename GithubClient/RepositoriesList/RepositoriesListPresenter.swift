//
//  RepositoriesListPresenter.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public enum ModuleType {
    case global
    case my
}

protocol RepositoriesListPresenterProtocol {
    var moduleType: ModuleType { get }
    var dataSource: [RepositoryDTO] { get }
    func reset()
    func showDetail(_ repository: RepositoryDTO)
    func showOwnList()
    func loadDataIfNeeded()
}

final class RepositoriesListPresenter: NSObject, RepositoriesListPresenterProtocol {
    
    var interactor: RepositoriesListInteractorProtocol!
    var wireframe: RepositoriesListWireframeProtocol!
    var view: RepositoriesListViewProtocol!
    
    var dataSource: [RepositoryDTO] = []
    public var moduleType: ModuleType = .global
    
    public func loadDataIfNeeded() {
        if moduleType == .my {
            load()
        }
    }
    
    public func load(name: String) {
        view.showLoader()
        interactor.loadRepositories(name: name) { [weak self] (items, error) in
            guard let `self` = self else { return }
            self.view.dismissLoader()
            self.rebuildDataSource(items: items, error: error)
        }
    }
    
    public func load() {
        view.showLoader()
        interactor.loadRepositories { [weak self] (items, error) in
            guard let `self` = self else { return }
            self.view.dismissLoader()
            self.rebuildDataSource(items: items, error: error)
        }
    }
    
    private func rebuildDataSource(items: [RepositoryDTO]?, error: NSError?) {
        
        if let error = error {
            
            // TODO: Show Error view
            
            print(error.domain)
            return
        }
        
        self.dataSource.removeAll()
        
        guard let dataItems = items else {
            self.view.reload()
            return
        }
        
        DispatchQueue.main.async {
            for item in dataItems {
                self.dataSource.append(item)
            }
            self.view.reload()
        }
    }
    
    public func reset() {
        DispatchQueue.main.async {
            self.dataSource.removeAll()
            self.view.reload()
        }
    }
    
    public func showDetail(_ repository: RepositoryDTO) {
        if repository.forksCount > 0 {
            wireframe.showDetail(repository)
        }
    }
    
    public func showOwnList() {
        if interactor.isAuthorizedUser() {
            wireframe.showOwnList()
        } else {
            wireframe.showLogin(delegate: self)
        }
    }
}

extension RepositoriesListPresenter: LoginDelegate {
    
    public func onTapCancel() {
        // 
    }
    
    public func onSuccessLogin() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.wireframe.showOwnList()
        }
    }
}
