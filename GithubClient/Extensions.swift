//
//  Extensions.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import Foundation
import UIKit

public extension Collection {
    public subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

public extension UIImageView {
    public func circle() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
}

public extension String {
    static func load(from property: String) -> String {
        if let path = Bundle.main.path(forResource: "Services-Info", ofType: "plist") {
            if let dimension1 = NSDictionary(contentsOfFile: path) {
                if let dimension2 = dimension1[property] as? String {
                    return dimension2
                }
            }
        }
        
        fatalError("Can't load the \(property) property. Make sure it is present")
    }
}

