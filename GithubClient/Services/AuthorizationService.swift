//
//  AuthorizationService.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol AuthorizationServiceProtocol {
    func isAuthorizedUser() -> Bool
    func token() -> String?
    func login() -> String?
    @discardableResult func save(token: String) -> Bool
    @discardableResult func save(login: String) -> Bool
}

public class AuthorizationService: NSObject, AuthorizationServiceProtocol {
    
    fileprivate lazy var dataService: AuthorizationDataService = AuthorizationDataService()
    
    public init(dataService: AuthorizationDataService = AuthorizationDataService()) {
        super.init()
        self.dataService = dataService
    }
    
    public func isAuthorizedUser() -> Bool {
        return dataService.token() != nil ? true : false
    }
    
    public func token() -> String? {
        return dataService.token()
    }
    
    @discardableResult public func save(token: String) -> Bool {
        return dataService.save(token: token)
    }
    
    public func login() -> String? {
        return dataService.login()
    }
    
    @discardableResult public func save(login: String) -> Bool {
        return dataService.save(login: login)
    }
}
