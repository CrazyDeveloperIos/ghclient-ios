//
//  AuthorizationDataService.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
import KeychainSwift

public class AuthorizationDataService: NSObject {
    
    fileprivate let keychain = KeychainSwift()
    fileprivate let tokenIdentificator = "users_token"
    fileprivate let loginIdentificator = "users_login"
    
    public func token() -> String? {
        return keychain.get(tokenIdentificator)
    }
    
    @discardableResult public func save(token: String) -> Bool {
        return keychain.set(token, forKey: tokenIdentificator)
    }
    
    @discardableResult public func delete(token: String) -> Bool {
        return keychain.delete(tokenIdentificator)
    }
    
    public func login() -> String? {
        return keychain.get(tokenIdentificator)
    }
    
    @discardableResult public func save(login: String) -> Bool {
        return keychain.set(login, forKey: tokenIdentificator)
    }
    
    @discardableResult public func delete(login: String) -> Bool {
        return keychain.delete(tokenIdentificator)
    }
}
