//
//  ForkDecodable.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public struct ForkDecodable: Decodable {
    public var id: Int?
    public var node_id: String?
    public var name: String?
    public var full_name: String?
    public var `private`: Bool?
    public var owner: RepoOwnerDecodable?
}
