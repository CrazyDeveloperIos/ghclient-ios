//
//  AuthorizationDecodable.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/21/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public struct AuthorizationDecodable: Decodable {
    public var access_token: String?
}
