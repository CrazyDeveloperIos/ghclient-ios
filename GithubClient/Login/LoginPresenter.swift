//
//  LoginPresenter.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/20/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

@objc public protocol LoginDelegate: class {
    @objc optional func onTapCancel()
    @objc optional func onSuccessLogin()
}

public protocol LoginPresenterProtocol {
    func loadUser(code: String)
    func loginUrl() -> URL?
    func dismiss()
}

public class LoginPresenter: NSObject, LoginPresenterProtocol {

    var interactor: LoginInteractorProtocol!
    var wireframe: LoginWireframeProtocol!
    var view: LoginViewProtocol!
    
    weak var delegate: LoginDelegate?
    
    public func loadUser(code: String) {
        
        view.showLoader()
        interactor.getUserLogin(code: code) { [weak self] (userLogin, error) in
            guard let `self` = self else { return }
            self.view.dismissLoader()
            if let _ = userLogin {
                self.showOwnList()
            } else {
                self.dismiss()
            }
        }
    }
    
    public func loginUrl() -> URL? {
        return interactor.loginUrl()
    }
    
    public func dismiss() {
        delegate?.onTapCancel?()
        wireframe.dismiss()
    }
    
    public func showOwnList() {
        delegate?.onSuccessLogin?()
        wireframe.dismiss()
    }
}
