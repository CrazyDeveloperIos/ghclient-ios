//
//  LoginWireframe.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/20/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol LoginWireframeProtocol: BaseWireframe {
    func dismiss()
}

public class LoginWireframe: NSObject, LoginWireframeProtocol {
    
    public var navigationController: UINavigationController!
    
    public func build(parameter: AnyObject?) -> UIViewController {
        
        let viewController = loginViewController()
        let presenter = LoginPresenter()
        let interactor = LoginInteractor()
        
        if let delegate = parameter as? LoginDelegate {
            presenter.delegate = delegate
        }
        
        viewController.presenter = presenter
        presenter.wireframe = self
        presenter.interactor = interactor
        presenter.view = viewController
        
        return viewController
    }
    
    private func loginViewController() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    }
    
    public func dismiss() {
        navigationController.dismiss(animated: true, completion: nil)
    }

}
