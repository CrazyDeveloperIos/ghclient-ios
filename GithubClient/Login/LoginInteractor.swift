//
//  LoginInteractor.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/20/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol LoginInteractorProtocol {
    func getUserLogin(code: String, completionHandler: @escaping (String?, NSError?) -> Void)
    func loginUrl() -> URL?
}

public class LoginInteractor: NSObject, LoginInteractorProtocol {
    
    private var dataProvider: AuthorizationDataProviderProtocol = AuthorizationDataProvider()
    
    public init(_ dataProvider: AuthorizationDataProviderProtocol = AuthorizationDataProvider()) {
        self.dataProvider = dataProvider
    }
    
    public func getUserLogin(code: String, completionHandler: @escaping (String?, NSError?) -> Void) {
        dataProvider.getUserLogin(code: code) { (login, error) in
            completionHandler(login, error)
        }
    }
    
    public func loginUrl() -> URL? {
        return dataProvider.loginUrl()
    }
}
