//
//  LoginViewController.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/20/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
import WebKit

public protocol LoginViewProtocol: Loader {
    
}

public class LoginViewController: UIViewController, LoginViewProtocol, WKNavigationDelegate {
    
    var presenter: LoginPresenterProtocol!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.setupWebView()
    }
    
    func setupNavigationBar() {
        let item = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(close))
        navigationItem.setRightBarButton(item, animated: true)
    }
    
    @objc func close() {
        dismissLoader()
        presenter.dismiss()
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        
        if let url = navigationAction.request.url, url.host == "callback.com" {
            if let code = url.query?.components(separatedBy: "code=").last {
                DispatchQueue.main.async {
                    self.presenter.loadUser(code: code)
                }
                
                decisionHandler(.cancel)
            }
            return
        }
        
        decisionHandler(.allow)
    }
    
    private func setupWebView() {
        
        let configuration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: configuration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        view.addSubview(webView)
        
        [webView.topAnchor.constraint(equalTo: view.topAnchor),
         webView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
         webView.leftAnchor.constraint(equalTo: view.leftAnchor),
         webView.rightAnchor.constraint(equalTo: view.rightAnchor)].forEach  { anchor in
            anchor.isActive = true
        }
        
        if let url = presenter.loginUrl() {
            let req = URLRequest(url: url)
            webView.load(req)
        }
    }
}
