//
//  RepositoriesNetworkService.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol RepositoriesNetworkServiceProtocol {
    func loadRepositories(name: String, completionHandler: @escaping (RepoDecodable?, NSError?) -> Void)
    func loadRepositories(login: String, completionHandler: @escaping ([RepoItemDecodable]?, NSError?) -> Void)
}

public class RepositoriesNetworkService: NetworkService, RepositoriesNetworkServiceProtocol {
    
    public func loadRepositories(name: String, completionHandler: @escaping (RepoDecodable?, NSError?) -> Void) {
        
        guard let query = name.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
            completionHandler(nil, NSError.init(domain: NetworkServiceError.badUrlNetworkDomain.rawValue, code: 0, userInfo: nil))
            return
        }
        
        let url = "\(api_url)/search/repositories?q=\(query)"
        
        requestManager.cancelPrevious(url: "\(api_url)/search/repositories?q=")
        
        requestManager.request(url: url, method: .get, parameters: nil) { (repo: RepoDecodable?, error: NSError?) in
            completionHandler(repo, error)
        }
    }
    
    public func loadRepositories(login: String, completionHandler: @escaping ([RepoItemDecodable]?, NSError?) -> Void) {
        
        let url = "\(api_url)/users/\(login)/repos"
        
        requestManager.request(url: url, method: .get, parameters: nil) { (repos: [RepoItemDecodable]?, error: NSError?) in
            completionHandler(repos, error)
        }
    }
}
