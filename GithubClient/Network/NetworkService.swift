//
//  NetworkService.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

public enum NetworkServiceError: String {
    case networkDomain = "NetworkErrorDomain"
    case badResponceNetworkDomain = "BadResponceErrorDomain"
    case badObjectModelNetworkDomain = "BadObjectModelErrorDomain"
    case badUrlNetworkDomain = "BadUrlErrorDomain"
}

open class NetworkService: NSObject {
    
    public var requestManager: RequestManager
    
    public init(requestManager: RequestManager = DefaultRequest()) {
        self.requestManager = requestManager
    }
}
