//
//  ForksNetworkService.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol ForksNetworkServiceProtocol {
    func loadForks(forksUrl: String, completionHandler: @escaping ([ForkDecodable]?, NSError?) -> Void)
}

public class ForksNetworkService: NetworkService, ForksNetworkServiceProtocol {
    
    public func loadForks(forksUrl: String, completionHandler: @escaping ([ForkDecodable]?, NSError?) -> Void) {
        requestManager.request(url: forksUrl, method: .get, parameters: nil) { (result: [ForkDecodable]?, error: NSError?) in
            completionHandler(result,error)
        }
    }
}
