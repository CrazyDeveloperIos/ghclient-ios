//
//  AuthorizationNetworkService.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/20/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
import Alamofire

public protocol AuthorizationNetworkServiceProtocol {
    func downloadAccessToken(code: String, completionHandler: @escaping (String?, NSError?) -> Void)
    func getUser(accessToken: String, completionHandler: @escaping (UserDecodable?, NSError?) -> Void)
}

public class AuthorizationNetworkService: NetworkService, AuthorizationNetworkServiceProtocol {
    
    public func downloadAccessToken(code: String, completionHandler: @escaping (String?, NSError?) -> Void) {
        
        let url = "\(web_url)/login/oauth/access_token"

        let parameters = ["client_id" : api_clientId,
                          "client_secret" : api_secretKey,
                          "code" : code]
    
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        requestManager.request(url: url, method: .post, parameters: parameters, headers: headers) { (auth: AuthorizationDecodable?, error: NSError?) in
            completionHandler(auth?.access_token, error)
        }
    }
    
    public func getUser(accessToken: String, completionHandler: @escaping (UserDecodable?, NSError?) -> Void) {
        
        let url = "\(api_url)/user"
        
        let headers: HTTPHeaders = [
            "Authorization": "token \(accessToken)",
            "Accept": "application/json"
        ]
        
        requestManager.request(url: url, method: .get, parameters: nil, headers: headers) { (user: UserDecodable?, error: NSError?) in
            completionHandler(user, error)
        }
    }
}
