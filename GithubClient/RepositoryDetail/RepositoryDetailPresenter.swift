//
//  RepositoryDetailPresenter.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

protocol RepositoryDetailPresenterProtocol {
    func title() -> String?
    var dataSource: [ForkDTO] { get}
}

class RepositoryDetailPresenter: NSObject, RepositoryDetailPresenterProtocol {
    
    var interactor: RepositoryDetailInteractorProtocol!
    var wireframe: RepositoryDetailWireframeProtocol!
    var view: RepositoryDetailViewProtocol! {
        didSet {
            forks()
        }
    }
    
    var dataSource: [ForkDTO] = []
    
    public func title() -> String? {
        return interactor.repo.repoName
    }
    
    public func forks() {
        view.showLoader()
        interactor.loadForks { [weak self] (forks, error) in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.view.dismissLoader()
                
                if let items = forks {
                    self.dataSource = items
                    self.view.reload()
                }
            }
        }
    }
}
