//
//  RepositoryDetailInteractor.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

protocol RepositoryDetailInteractorProtocol {
    var repo: RepositoryDTO { get }
    func loadForks(completionHandler: @escaping ([ForkDTO]?, NSError?) -> Void)
}

class RepositoryDetailInteractor: NSObject, RepositoryDetailInteractorProtocol {
    
    var repo: RepositoryDTO
    
    init(repo: RepositoryDTO) {
        self.repo = repo
    }
    
    private var forksDataProvider: ForksDataProviderProtocol = ForksDataProvider()
    
    public func set(_ dataProvider: ForksDataProviderProtocol) {
        forksDataProvider = dataProvider
    }
    
    public func loadForks(completionHandler: @escaping ([ForkDTO]?, NSError?) -> Void) {
        
        guard let forks_url = repo.forks_url else { return }
        
        forksDataProvider.loadForks(forksUrl: forks_url) { (forks, error) in
            completionHandler(forks, error)
        }
    }
}
