//
//  RepositoryDetailViewController.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

protocol RepositoryDetailViewProtocol: Loader {
    func reload()
}

class RepositoryDetailViewController: UITableViewController, RepositoryDetailViewProtocol {

    var presenter: RepositoryDetailPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = presenter.title()
        self.tableView.tableFooterView = UIView()
    }
    
    public func reload() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

// MARK: - Table view data source

extension RepositoryDetailViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryDetailTableViewCell", for: indexPath) as! RepositoryDetailTableViewCell
        guard let item = presenter.dataSource[safe: indexPath.row] else { return cell }
        cell.setup(fork: item)
        
        return cell
    }
}
