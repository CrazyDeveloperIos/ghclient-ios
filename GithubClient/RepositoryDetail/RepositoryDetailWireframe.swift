//
//  RepositoryDetailWireframe.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

protocol RepositoryDetailWireframeProtocol: BaseWireframe {
    
}

class RepositoryDetailWireframe: NSObject, RepositoryDetailWireframeProtocol {
    
    var navigationController: UINavigationController!

    public func build(parameter: AnyObject? = nil) -> UIViewController {
        
        guard let repo = parameter as? RepositoryDTO else { fatalError("You need to pass a `RepositoryDTO` object") }
        
        let viewController = buildViewController()
        let presenter = RepositoryDetailPresenter()
        let interactor = RepositoryDetailInteractor(repo: repo)
        
        viewController.presenter = presenter
        
        presenter.wireframe = self
        presenter.interactor = interactor
        presenter.view = viewController
        
        return viewController
    }
    
    private func buildViewController() -> RepositoryDetailViewController {
        let storyboard = UIStoryboard(name: "RepositoryDetail", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "RepositoryDetailViewController") as! RepositoryDetailViewController
    }
}
