//
//  RepositoryDetailTableViewCell.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

class RepositoryDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        icon.circle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(fork: ForkDTO) {
        
        self.title.text = fork.owner_login
        guard let urlStr = fork.owner_avatar_url,
            let url = URL(string: urlStr) else {
                icon.image = UIImage(named: "dev")
                return
        }
        
        icon?.sd_setImage(with: url, placeholderImage: UIImage(named: "dev"), options: .progressiveDownload)
    }

}
