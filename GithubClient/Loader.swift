//
//  Loader.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/22/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit
import SVProgressHUD

public protocol Loader {
    func showLoader()
    func dismissLoader()
}

public extension Loader where Self: UIViewController {
    public func showLoader() {
        SVProgressHUD.show()
    }
    
    public func dismissLoader() {
        SVProgressHUD.dismiss()
    }
}
