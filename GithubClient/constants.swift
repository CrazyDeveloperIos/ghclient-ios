//
//  constants.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/21/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import Foundation

public let api_clientId = String.load(from: "clientId")
public let api_secretKey = String.load(from: "clientSecret")
public let api_url = String.load(from: "api_url")
public let web_url = String.load(from: "web_url")

