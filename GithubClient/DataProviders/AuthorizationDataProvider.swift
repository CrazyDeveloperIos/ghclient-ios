//
//  AuthorizationDataProvider.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/21/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol AuthorizationDataProviderProtocol {
    func loginUrl() -> URL?
    func getUserLogin(code: String, completionHandler: @escaping (String?, NSError?) -> Void)
}

public class AuthorizationDataProvider: NSObject, AuthorizationDataProviderProtocol {
    
    private var authorizationNetworkService: AuthorizationNetworkServiceProtocol = AuthorizationNetworkService()
    private var authorizationService: AuthorizationServiceProtocol = AuthorizationService()
   
    public func setAuthorizationNetworkService(service: AuthorizationNetworkServiceProtocol) {
        authorizationNetworkService = service
    }
    
    public func setAuthorizationService(service: AuthorizationServiceProtocol) {
        authorizationService = service
    }
    
    public func isAuthorizedUser() -> Bool {
        return authorizationService.isAuthorizedUser()
    }
    
    public func loginUrl() -> URL? {
        return URL(string: "\(web_url)/login/oauth/authorize?client_id=\(api_clientId)")
    }
    
    public func getUserLogin(code: String, completionHandler: @escaping (String?, NSError?) -> Void) {
        if authorizationService.isAuthorizedUser() {
            completionHandler(authorizationService.token(), nil)
            return
        }
        
        loadUser(code: code) { (user, error) in
            if let userUnwrapped = user {
                completionHandler(userUnwrapped.login, nil)
            } else {
                completionHandler(nil, error)
            }
        }
    }
    
    private func loadUser(code: String, completionHandler: @escaping (UserDTO?, NSError?) -> Void) {
        
        if !authorizationService.isAuthorizedUser() {
            authorizationNetworkService.downloadAccessToken(code: code) { [weak self] (accessToken, error) in
                
                guard let `self` = self else { return }
                guard let token = accessToken else {
                    completionHandler(nil, error)
                    return
                }
                
                self.authorizationService.save(token: token)
                self.authorizationNetworkService.getUser(accessToken: token, completionHandler: { [weak self] (userDecodable, error) in
                    
                    if let userDecodableUnwrapped = userDecodable {
                        let user = UserDTO(userDecodableUnwrapped)
                        guard let `self` = self else { return }
                        
                        if let login = user.login {
                            self.authorizationService.save(login: login)
                        }
                    
                        completionHandler(user, error)
                    }
                })
            }
        }
    }
}
