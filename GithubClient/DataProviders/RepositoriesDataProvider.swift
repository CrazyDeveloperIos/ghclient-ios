//
//  RepositoriesDataProvider.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol RepositoriesDataProviderProtocol {
    func loadRepositories(name: String, completionHandler: @escaping ([RepositoryDTO]?, NSError?) -> Void)
    func loadRepositories(login: String, completionHandler: @escaping ([RepositoryDTO]?, NSError?) -> Void)
}

public class RepositoriesDataProvider: NSObject, RepositoriesDataProviderProtocol {
    
    private var repositoriesNetworkService: RepositoriesNetworkServiceProtocol
    
    public init(_ service: RepositoriesNetworkServiceProtocol = RepositoriesNetworkService()) {
        repositoriesNetworkService = service
    }
    
    public func loadRepositories(name: String, completionHandler: @escaping ([RepositoryDTO]?, NSError?) -> Void) {
        repositoriesNetworkService.loadRepositories(name: name) { [weak self] (repositories, error) in
            
            guard let `self` = self else { return }
            
            if let repositoryUnwrapped = repositories {
                completionHandler(self.buildTransfer(items: repositoryUnwrapped.items), nil)
            } else {
                completionHandler(nil, error)
            }
        }
    }
    
    public func loadRepositories(login: String, completionHandler: @escaping ([RepositoryDTO]?, NSError?) -> Void) {
        repositoriesNetworkService.loadRepositories(login: login) { [weak self] (repositories, error) in
            
            guard let `self` = self else { return }
            
            if let repositoriesUnwrapped = repositories {
                completionHandler(self.buildTransfer(items: repositoriesUnwrapped), nil)
            } else {
                completionHandler(nil, error)
            }
        }
    }
    
    private func buildTransfer(items: [RepoItemDecodable]?) -> [RepositoryDTO] {
        guard let items = items else { return [] }
        return items.compactMap { (item) -> RepositoryDTO in
            return RepositoryDTO(item)
        }
    }
}
