//
//  ForksDataProvider.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

protocol ForksDataProviderProtocol {
    func loadForks(forksUrl: String, completionHandler: @escaping ([ForkDTO]?, NSError?) -> Void)
}

public class ForksDataProvider: NSObject, ForksDataProviderProtocol {
    
    private var forksNetworkService: ForksNetworkServiceProtocol
    
    public init(_ service: ForksNetworkServiceProtocol = ForksNetworkService()) {
        forksNetworkService = service
    }
    
    public func loadForks(forksUrl: String, completionHandler: @escaping ([ForkDTO]?, NSError?) -> Void) {
        forksNetworkService.loadForks(forksUrl: forksUrl) { (forks, error) in
            if let forksUnwrapped = forks {
                
                let forksData: [ForkDTO] = forksUnwrapped.compactMap({ (item) -> ForkDTO in
                    return ForkDTO(item)
                })
                
                completionHandler(forksData, nil)
            }
            
            completionHandler(nil, error)
        }
    }
}
