//
//  LoadingWireframe.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public protocol BaseWireframe {
    func build(parameter: AnyObject?) -> UIViewController
    var navigationController: UINavigationController! { set get }
}

public class LoadingWireframe: NSObject {
    public func installRootViewControllerIntoWindow(window: UIWindow) {
        window.rootViewController = loadDialog(window: window)
        window.makeKeyAndVisible()
    }
    
    private func loadDialog(window: UIWindow) -> UINavigationController {
        var wireframe: RepositoriesListWireframeProtocol = RepositoriesListWireframe()
        let navigationController = UINavigationController(rootViewController: wireframe.build(parameter: nil))
        wireframe.navigationController = navigationController
        return navigationController
    }
}
