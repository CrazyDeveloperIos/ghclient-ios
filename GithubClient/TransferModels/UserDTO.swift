//
//  UserDTO.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/21/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

 public class UserDTO: NSObject {
    
    public var login: String?
    public var id: Int?
    public var avatar_url: String?
    public var repos_url: String?
    
    public init(_ user: UserDecodable) {
        super.init()
        self.login = user.login
        self.id = user.id
        self.avatar_url = user.avatar_url
        self.repos_url = user.repos_url
    }
}
