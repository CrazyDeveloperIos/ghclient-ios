//
//  RepositoryDTO.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/18/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public class RepositoryDTO: NSObject {
    public var ownerImage: String?
    public var repoName: String?
    public var repoDescription: String?
    public var watchersCount: Int = 0
    public var forksCount: Int = 0
    public var forks_url: String?
    
    public init(_ repo: RepoItemDecodable) {
        self.ownerImage = repo.owner?.avatar_url
        self.repoName = repo.name
        self.repoDescription = repo.description
        self.watchersCount = repo.watchers_count ?? 0
        self.forksCount = repo.forks_count ?? 0
        self.forks_url = repo.forks_url
    }
}
