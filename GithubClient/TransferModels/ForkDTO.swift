//
//  ForkDTO.swift
//  GithubClient
//
//  Created by Nazar Gorobets on 10/19/18.
//  Copyright © 2018 nazar.gorobets. All rights reserved.
//

import UIKit

public class ForkDTO: NSObject {
    public var id: Int?
    public var name: String?
    public var full_name: String?
    public var owner_login: String?
    public var owner_avatar_url: String?
    
    public init(_ fork: ForkDecodable) {
        self.id = fork.id
        self.name = fork.name
        self.full_name = fork.full_name
        self.owner_login = fork.owner?.login
        self.owner_avatar_url = fork.owner?.avatar_url
    }
}
